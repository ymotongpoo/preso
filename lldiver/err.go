func Foo() (string, error) {
        if !success {
                return "", fmt.Errorf("Error occured")
        }
        return "foo", nil
}

func Bar() (string, error) {
        result, err := Foo()
        if err != nil {
                return "", fmt.Errorf("[Bar]: %v", err)
        }
        return result+"bar", nil
}
