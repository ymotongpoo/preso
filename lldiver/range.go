package main

import "fmt"

func main() {
    a := []int{100, 200, 300}
    for i, v := range a {
        fmt.Printf("%vth number: %v\n", i, v)
    }
}
