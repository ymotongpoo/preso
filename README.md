#Overview

repo for preso.ymotongpoo.com

#Setup

##1. setup nginx

add following section in default

```
server {
        server_name preso.ymotongpoo.com;
        listen 80;

        location / {
                proxy_pass http://127.0.0.1:3999/;
        }
}
```

##2. setup go.tools

download 'present' command from go.tools

```
$ go get code.google.com/p/go.tools/cmd/present
```

##3. download this repo

```
$ cd $GOPAH/src
$ git clone https://bitbucket.org/ymotongpoo/preso .
```

##4. launch present

start present server

```
$ present
```
