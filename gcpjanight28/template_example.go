import "html/template"

type Employee struct {
    ID   int
    Name string
}

employeeData := EmployeeList()
data := struct {
    Employees []Employee
    Results   int
}{
    Employees: employeeData,
    Results: len(employeeData),
}

t, err := template.New("foo").ParseFiles("main.html", "footer.html") // HL
err = t.Execute(w, data) // HL
