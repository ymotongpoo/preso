package hello

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// START OMIT
func init() {
	r := mux.NewRouter()    // Routerを作成する
	r.HandleFunc("/", root) // ハンドラーを設定する
	http.Handle("/", r)     // net/httpでリクエストを処理
}

func root(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "こんにちは世界! from Gorilla")
}

// END OMIT
