package hello

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// START OMIT
func init() {
	r := gin.New()      // ミドルウェアなしでGinのインスタンスを作成
	r.GET("/", root)    // ハンドラーの設定
	http.Handle("/", r) // net/httpでリクエストの処理
}

func root(c *gin.Context) {
	c.String(200, "こんにちは世界! form Gin")
}

// END OMIT
