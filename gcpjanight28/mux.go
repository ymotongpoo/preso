func ListenAndServe(addr string, handler Handler) error

type Handler interface {
	ServeHTTP(ResponseWriter, *Request) // HL
}

type ServeMux struct {
}

func (*ServeMux) Handle(pattern string, handler Handler)

func (*ServeMux) HandleFunc(pattern string, handler func(ResponseWriter, *Request))

func (*ServeMux) Handler(r *Request) (h Handler, pattern string)

func (*ServeMux) ServeHTTP(w ResponseWriter, r *Request) // HL
