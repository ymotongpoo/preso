package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", myHandler)   // ハンドラーの設定
	http.ListenAndServe(":8080", nil) // リスナーの起動
}

func myHandler(w http.ResponseWriter, r *http.Request) {
	origin := r.RemoteAddr
	fmt.Fprintf(w, "こんにちは世界! from "+origin)
}
