package hello

import (
	"fmt"
	"net/http"
)

// init()関数しかないことに注意
func init() { // HL
	http.HandleFunc("/", handler)
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hello")
}
