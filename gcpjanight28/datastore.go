import (
    ...
    "appengine"
    "appengine/datastore"
)

func f(w http.ResponseWriter, r *http.Request) {
    c := appengine.NewContext(r)
    employee := &Employee {
        Name: "やまぐちよしふみ",
        Age: 30,
    }
    key := datastore.NewIncompleteKey(c, "employee", nil)
    _, err := datastore.Put(c, key, employee)
}
