package main

import (
    "fmt"
    "net/http"
)

func main() {
    http.HandleFunc("/", handler) // URLディスパッチ
    http.ListenAndServe(":8080", nil) // サーバ起動
}

func handler(w http.ResponseWriter, r *http.Requset) { // インターフェースは決まっている
    fmt.Fprintf(w, "こんにちは、世界")
}
