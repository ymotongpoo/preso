package sample1

type Hoge struct {
    name string
}

type Bar struct {
    num int
}

func Foo(h Hoge) Bar {
    return Bar{len(h.name)}
}
