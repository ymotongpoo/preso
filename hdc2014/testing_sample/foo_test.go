package sample1

import (
    "reflect"
    "testing"
)

func TestFoo(t *testing.T) {
    in := []Hoge{Hoge{"gopher"}, Hoge{"hakodate"}, Hoge{"hdc2014"}}
    wants := []Bar{Bar{6}, Bar{2}, Bar{7}}
    outs := make([]Bar, len(wants))
    for i, h := range in {
        outs[i] = Foo(h)
    }
    if !reflect.DeepEqual(wants, outs) {
        t.Errorf("wants: %v, outs: %v", wants, outs)
    }
}
