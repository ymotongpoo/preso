package main

import (
    "flag"
    "fmt"
    "io"
    "os"
)

// START flag_sample_OMIT
var (
    input  = flag.String("input", "input.txt", "input filename")
    output = flag.String("output", "output.txt", "output filename")
)

func main() {
    infile, err := os.Open(*input)
    if err != nil {
        panic(err)
    }
    outfile, err := os.Create(*output)
    if err != nil {
        panic(err)
    }
    n, err := io.Copy(outfile, infile)
    if err != nil {
        panic(err)
    }
    fmt.Printf("Copied %v bytes from %v to %v", n, *input, *output)
}
// END flag_sample_OMIT
