package main

import (
	"fmt"
	"math"
)

// START ダメな型_OMIT
type Celsius float64
type Fahrenheit float64

// cとfの型が違うのでコンパイル時エラー
func SomeFunc() Fahrenheit {
	c := Celsius(100)
	f := Fahrenheit(20)
	return c + f // HL
}
// END ダメな型_OMIT

// START OKな型_OMIT
type Celsius float64
type Fahrenheit float64

func (c Celsius) ToF() Fahrenheit {
	return Fahrenheit((9*c/5)+32)
}

// cとfの型が合ってるのでOK
func SomeFunc() Fahrenheit {
	c := Celsius(100)
	f := Fahrenheit(20)
	return c.ToF() + f // HL
}
// END OKな型_OMIT

// START 冗長_OMIT
var c1 Celsius
var c2 Celsius

c1 = Celsius(100)
c2 = Celsius(2)

var c Celsius
c = c1 + c2
// END 冗長_OMIT

// START 簡潔_OMIT
c1 := Celsius(100)
c2 := Celsius(2)

c := c1 + c2
// END 簡潔_OMIT

// START func_OMIT
// Goには例外処理が無いので、正常値と異常値のタプルを返す。異常値はerrorの実装。
func PickByte(s string, n int) (byte, error) {
	if n >= len(s) {
		return byte(0), errors.New("out of range")
	}
	return s[n], nil
}
// END func_OMIT
// START funcvar_OMIT
var f func(string, int) (byte, error)
f = PickByte
// END funcvar_OMIT

// START struct_OMIT
type Rectangle struct {
	Width  float64 // フィールドにはフィールド名とその型を記述
	Length float64
}

// メソッドはレシーバの記述がある
func (r Rectangle) Area() float64 {
	return r.Width * r.Length
}
// END struct_OMIT

// START interface_OMIT
type Shaper interface {
	Area() float64
}

type Rectangle struct {
	Width  float64
	Length float64
}

func (r Rectangle) Area() float64 { // Shaperで定義されているメソッド
	return r.Width * r.Length
}
// END interface_OMIT
// START interface2_OMIT
type Square struct {
	Length float64
}

func (sq Square) Area() float64 { // Shaperで定義されているメソッド
	return sq.Length * sq.Length
}

type Circle struct {
	Radius float64
}

func (c Circle) Area() float64 {
	return math.Pi * c.Radius * c.Radius
}
// END interface2_OMIT
// START interface3_OMIT
func main() {
	r := Rectangle{Width: 3, Length: 5}
	sq := Square{Length: 2}
	c := Circle{Radius: 3}

	// 全部Shapeにキャストできる
	shapes := []Shaper{Shaper(r), Shaper(sq), Shaper(c)} // HL

	for _, s := range shapes {
		fmt.Println(s.Area())
	}
}
// END interface3_OMIT

// START goroutine_OMIT
func SomeProcess(t Task) {}
	fmt.Println("process task...")

	// goキーワードでgoroutineを起動
	go func() { // HL
		DoTask(t)
	}()

	fmt.Println("process started...")
}

func DoTask(t Task) {
	...
	fmt.Println("task done!!")
}
// END goroutine_OMIT

// START channel_OMIT
timerChan := make(chan time.Time)
go func() {
    time.Sleep(deltaT)
    timerChan <- time.Now() // timerChanに時刻を入れる <-演算子でchannelへ代入
}()
// この隙に何か別のことができる。
// channelからのデータの受け取りはブロックする。
completedAt := <-timerChan // <-でchannelから取り出し
// END channel_OMIT

// START testing_OMIT
import (
	"reflect"
	"testing" // HL
)

func TestFoo(t *testing.T) {
	in := []Hoge{...}
	wants := []Bar{...}
	outs := make([]Bar, len(wants))
	for i, h := range in {
		outs[i] = Process(h)
	}
	if !reflect.DeepEqual(wants, outs) { // HL
		t.Errorf("wants: %v, out: %v", wants, outs)
	}
}
// END testing_OMIT
